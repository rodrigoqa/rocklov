# language: pt


Funcionalidade: Remover Arquivos
    Sendo um anunciante que possui um anúncio indesejado
    Quero poder remover este anúncio
    Para que eu possa manter meu Dashboard atualizado

    Contexto: Login
        * Login com "eu@eumesmo.com" e "123456"

     @teste
    Cenário: Remover anúncio

        Dado que eu tenho um anúncio indesejado:
            | thumb     | violino.jpg |
            | nome      | violino     |
            | categoria | Cordas      |
            | preco     | 975         |
        Quando eu solicito a exclusão deste item
            E confirmo a exclusão
        Então não devo ver este anúncio no meu Dashboard


   
    Cenário: Desistir da exclusão

        Dado que eu tenho um anúncio indesejado:
            | thumb     | clarinete.jpg |
            | nome      | clarinente    |
            | categoria | Outros        |
            | preco     | 654           |
        Quando eu solicito a exclusão deste item
            Mas não confirmo a exclusão
        Então esse item deve permanecer no meu Dashboard



