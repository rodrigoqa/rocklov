#language:pt

Funcionalidade: Receber Pedido de Locação
    Sendo um anunciante que possui equipamentos cadastrados
    Desejo receber pedidos de Locação
    Para decidir se quero aprová-los ou rejeitá-los
    @temp2
    Cenario: Receber Pedidos

        Dado que meu perfil de anunciante é "eu@eumesmo.com" e "123456"
            E tenho o seguinte equipamento cadastrado:
            | thumb     | trompete.jpg |
            | nome      | Trompete     |
            | categoria | Outros       |
            | preco     | 150          |
            E acesso meu Dashboard
        Quando "emaildeteste@hotmail.com" e "123456" solicita a locação desse equipo
        Então devo ver a seguinte mensagem
        """
        emaildeteste@hotmail.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
        """
        E devo ver os links "ACEITAR" e "REJEITAR" no pedido

