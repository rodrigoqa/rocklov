describe "POST /sessions" do
    context "login com sucesso" do
        before(:all) do
            payload = { email:"malandrovisck@hotmail.com", password: "123456"}
            @result = Sessions.new.login(payload) 
                
        end

        it "Valida status code" do
            expect(@result.code).to eql 200
        end

        it "Valida id do usuário" do
            expect(@result.parsed_response["_id"].length).to eql 24
        end
    end

    # examples = [
    #     {
    #         title: "Senha invalida",
    #         payload: { email:"nossailha@live.com", password: "Jabulani"},
    #         code: 401,
    #         error: "Unauthorized",
    #     },
    #     {
    #         title: "Usuario nao existe",
    #         payload: { email:"nosa@live.com", password: "Jabulani"},
    #         code: 401,
    #         error: "Unauthorized",
    #     },
    #     {
    #         title: "Email em branco",
    #         payload: { email:"", password: "Jabulani"},
    #         code: 412,
    #         error: "required email",
    #     },
    #     {
    #         title: "Sem email",
    #         payload: { password: "Jabulani"},
    #         code: 412,
    #         error: "required email",
    #     },
    #     {
    #         title: "Senha em branco",
    #         payload: { email:"nossailha@live.com", password: ""},
    #         code: 412,
    #         error: "required password",
    #     },
    #     {
    #         title: "Sem senha",
    #         payload: { email:"nossailha@live.com", password: ""},
    #         code: 412,
    #         error: "required password",
    #     }
    # ]
    examples = Helpers::get_fixture("login")

    examples.each do |e|
        context "#{e[:title]}" do
            before(:all) do
               
                @result = Sessions.new.login(e[:payload]) 
                    
            end
    
            it "Valida status code #{e[:code]}" do
                expect(@result.code).to eql e[:code]
            end
    
            it "Valida id do usuário" do
                expect(@result.parsed_response["error"]).to eql e[:error]
            end
        end
    end
   

  
end