describe "POST /equipos" do
    # esse bloco serve para captar o User Id do usuário através do Sessions.rb
    before(:all) do
        payload = { email: "topo@gigio.com", password: "123456"}
        result = Sessions.new.login(payload)
        @user_id = result.parsed_response["_id"]
    end
    
    context "novo equipo" do
        before (:all) do
                payload = { 
                thumbnail: Helpers::get_thumb("kramer.jpg"),
                name: "Kramer Eddie Van Halen",
                category: "Cordas",
                price: 299,
            }

            MongoDb.new.remove_equipo(payload[:name], @user_id)
            @result = Equipos.new.create(payload, @user_id)
            
        end

        it "deve retornar 200" do
            expect(@result.code).to eql 200
        end
    end

    context "no puede" do
        before (:all) do
                payload = { 
                thumbnail: Helpers::get_thumb("baixo.jpg"),
                name: "Baixo empenado",
                category: "Cordas",
                price: 199,
            }

            
            @result = Equipos.new.create(payload, nil)
            
        end

        it "deve retornar 401" do
            expect(@result.code).to eql 401
        end
    end
end
