module Helpers
    def get_fixture(item)
        YAML.load(File.read(Dir.pwd + "/spec/fixtures/#{item}.yml"), symbolize_names: true)
    end

    def get_thumb(file_name)
        # o "rb" informado no final do código serve para que a imagem seja carregada de
        # forma completa e não ir em branco
        return File.open(File.join(Dir.pwd, "spec/fixtures/images", file_name), "rb")
    end

    module_function :get_fixture
    module_function :get_thumb
end
