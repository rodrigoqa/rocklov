describe "POST /signup" do
    context "novo usuario" do
        before(:all) do
            payload = { name: "Chuchu", email: "chayote@live.com", password: "1234" }
            MongoDb.new.remove_user(payload[:email])
            @result = Signup.new.create(payload)
        end

        it "Valida status code" do
            expect(@result.code).to eql 200
        end

        it "Valida id do usuário" do
            expect(@result.parsed_response["_id"].length).to eql 24
        end

    end

    context "usuario ja existe" do
        before(:all) do
            # Dado que eu tenho um novo usuario
            payload = { name: "Água de Salsicha", email: "sausage@live.com", password: "1234" }
            MongoDb.new.remove_user(payload[:email])
            # eo emaill desse usuario ja foi cadastro no sistema
            Signup.new.create(payload)
            # quando faço a requisição para a rota/ signup
            @result = Signup.new.create(payload)
        end

        it "deve retornar 409" do
            # então deve retornar 409
            expect(@result.code).to eql 409
        end

        it "deve retornar mensagem" do
            expect(@result.parsed_response["error"]).to eql "Email already exists :("
        end
    end

    examples = [

        {
            title: "nome em branco",
            payload: {name:"", email: "sausage@live.com", password: "1234" },
            code: 412,
            error: "required name",
        },

        {
            title: "sem o nome",
            payload: {email:"sausage@live.com", password: "1234" },
            code: 412,
            error: "required name",
        },

        {
            title: "email em branco",
            payload: {name: "Água de Salsicha", email: "", password: "1234" },
            code: 412,
            error: "required email",
        },

        {
            title: "sem email",
            payload: {name: "Água de Salsicha", password: "1234" },
            code: 412,
            error: "required email",
        },

        {
            title: "email invalido",
            payload: {name: "Água de Salsicha", email: "chayote+live.com", password: "1234" },
            code: 412,
            error: "wrong email",
        },

        {
            title: "senha em branco",
            payload: {name: "Água de Salsicha", email: "chayote@live.com", password: "" },
            code: 412,
            error: "required password",
        },

        {
            title: "sem o campo senha",
            payload: {name: "Água de Salsicha", email: "chayote@live.com" },
            code: 412,
            error: "required password",
        }


    ]

    examples.each do |e|
        context "#{e[:title]}" do
            before(:all) do
               
                @result = Signup.new.create(e[:payload]) 
                    
            end
    
            it "Valida status code #{e[:code]}" do
                expect(@result.code).to eql e[:code]
            end
    
            it "Valida mensagem de erro" do
                expect(@result.parsed_response["error"]).to eql e[:error]
            end
        end
    end
   
end

