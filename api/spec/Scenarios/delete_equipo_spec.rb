#encoding: utf-8

    describe "DELETE /equipo/{equipos_id" do

            before(:all) do
                @payload = { email: "topo@gigio.com", password: "123456"}
                result = Sessions.new.login(@payload)
                @user_id = result.parsed_response["_id"]
            end

            context "obter unico equipo" do
        
            before(:all) do
                #dado que eu tenho um novo equipamento
                @payload = { 
                    thumbnail: Helpers::get_thumb("pedais.jpg"),
                    name: "Pedais do seu Tomais",
                    category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"),
                    price: 1000,
                }   
                MongoDb.new.remove_equipo(@payload[:name], @user_id)
                # e eu tenho  o id desse equipamento
                equipo = Equipos.new.create(@payload, @user_id)
                @equipo_id = equipo.parsed_response["_id"]

                #quando eu precisar fazer uma requisição GET por id
                @result = Equipos.new.remove_by_id(@equipo_id, @user_id)


            end

                it "deve retornar 204" do
                expect(@result.code).to eql 204
            end

           

        end

        context "equipo nao existe" do
            before(:all) do
               @result = Equipos.new.remove_by_id(MongoDb.new.get_mongo_id, @user_id)
            end

            it "deve retornar 204" do
                expect(@result.code).to eql 204
            end
        end

    
end