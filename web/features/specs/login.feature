#language: pt


Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login_valido
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "gege@hotmail.com" e "123456"
        Então sou redirecionado para o Dashboard

    @login_invalido
    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:

            | email_input        | senha_input | mensagem_output                  |
            | jack@tequila.com   | 4321        | Usuário e/ou senha inválidos.    |
            | pepino@tequila.com | 1234        | Usuário e/ou senha inválidos.    |
            | jack*tequila.com   | 1234        | Oops. Informe um email válido!   |
            |                    | 1234        | Oops. Informe um email válido!   |
            | jack@tequila.com   |             | Oops. Informe sua senha secreta! |





