require_relative "routes/signup"
require_relative "routes/sessions"
require_relative "routes/equipos"

require_relative "libs/mongo"
require_relative "helpers"
#esse require serve para leitura de criptografia de senhas no modelo md5
require "digest/md5"


  def to_md5(pass) 

    return Digest::MD5.hexdigest(pass)

  end

RSpec.configure do |config|
  
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

    config.shared_context_metadata_behavior = :apply_to_host_groups

    config.before(:suite) do

      users = [
        {name: "Serio Malandro", email:"malandrovisck@hotmail.com", password:to_md5("123456")},
        {name: "Topo Gigio" , email:"topo@gigio.com", password:to_md5("123456")},
        {name: "Garoto Simforoso" , email:"picumam@ig.com", password:to_md5("123456")},
        {name: "Petrobras" , email:"ga@solina.com", password:to_md5("123456")},
        {name: "Salsicha & ScoobyDoo" , email:"jibimba@ig.com", password:to_md5("123456")},
      ]

      MongoDb.new.drop_danger
      MongoDb.new.insert_users(users)

    end

end
