    describe "POST /equipos/{equipo_id}/bookings" do

        before(:all) do
            payload = { email: "ga@solina.com", password: "123456"}
            result = Sessions.new.login(payload)
            @gasolina_id = result.parsed_response["_id"]
        end

        context "solicitar locacao" do

            before (:all) do

                # dado que o "Jibimba Jr" tem uma "Fender Strato" para locação

                result = Sessions.new.login({email: "jibimba@ig.com", password:"123456"})
                jibs_id =  result.parsed_response["_id"]

                fender = { 
                    thumbnail: Helpers::get_thumb("fender-sb.jpg"),
                    name: "Guitarra Fender",
                    category: "Cordas",
                    price: 150,
                }

                MongoDb.new.remove_equipo(fender[:name], jibs_id)

                result = Equipos.new.create(fender, jibs_id)
                fender_id = result.parsed_response["_id"]

                #quando solicito a fender do Jibimba
                @result = Equipos.new.booking(fender_id, @gasolina_id)

            end

            it "deve retornar 200" do

                expect(@result.code).to eql 200

            end

        end

    end